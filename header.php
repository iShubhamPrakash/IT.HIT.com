<?php
/* Created by Aditya Thakur 
linkedin: linkedin.com/adityathakur532
ALL RIGHTS RESERVED */
require("db.php");
session_start();
?>

<html>

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <?php echo $page_requirements; //All js files and page title will be passed to header through this variable.?>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/styles/header.css">
</head>

<body>
  <div class="header">
    <div class="navbar">
      <a href="index.php">Home</a>
      <div class="dropdown">
        <button class="dropbtn">People
          <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
          <a href="faculty.php">Faculty</a>
          <a href="students.php">Current Students</a>
          <a href="alumnis.php">Alumnis</a>
          <a href="staffs.php">Staffs</a>
        </div>
      </div>
      <a href="arc.php">ARC</a>
      <a href="gallery.php">Gallery</a>
      <div class="dropdown">
        <button class="dropbtn">About Us
          <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
          <a href="about_us.php">About College</a>
          <a href="about_us.php">About Department</a>
        </div>
      </div>
      <div class="dropdown">
        <button class="dropbtn">Community
          <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
          <a href="acm-chapter.php">ACM Chapter</a>
          <a href="coding-club.php">Coding Club</a>
          <a href="sports-club.php">Sports Club</a>
          <a href="cultural-club.php">Cultural Club</a>
        </div>
      </div>
      <a href="my_profile.php">My Profile</a>
    </div>


    <div class="it-hit">
      <a href="index.php"><img src="styles/images/it_logo.png" align="left" alt="itlogo"></a>
      <a href="index.php"><strong>
          <pre>Information Technology</pre></strong></a>
      <a href="hithaldia.in"><strong>
          <p>Haldia Institute of Technology</p>
        </strong></a>


      <?php

      if ( $_SESSION['logged_in'] != 1 ){

      echo '<a href="signin-up.php"><button>Login/Signup</button></a>';

      } else {
      echo '<a href="logout.php"><button>Logout</button></a>';

            }
      ?>

    </div>
  </div>