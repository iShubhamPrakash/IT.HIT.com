<?php
/*
 © aditya thakur ©Piyush Vikas
  contact: https://www.linkedin.com/in/aditya532/
 ALL RIGHTS RESERVED
 */
$page_requirements = '
	<title>ARC | IT Dept</title>
<link rel="stylesheet" href="styles/arc.css" type="text/css" />	';
 	include("header.php");
?>
<section class="arc-content">
	<div id="container_arc">
		<h1>Anti-Ragging Cell</h1>
			<p>
			<strong>Ragging constitutes one or more of any of the following acts:</strong><br><br>
			Any conduct by any student or students whether by words spoken or written or by an act which has the effect of teasing, treating or handling with rudeness a fresher or any other student;<br>
			Indulging in rowdy or undisciplined activities by any student or students which causes or is likely to cause annoyance, hardship, physical or psychological harm or to raise fear or apprehension thereof in any fresher or any other student;
			Asking any student to do any act which such student will not in the ordinary course do and which has the effect of causing or generating a sense of shame, or torment or embarrassment so as to adversely affect the physique or psyche of such fresher or any other student;<br>
			Any act by a senior student that prevents, disrupts or disturbs the regular academic activity of any other student or a fresher;
			Exploiting the services of a fresher or any other student for completing the academic tasks assigned to an individual or a group of students.
			Any act of financial extortion or forceful expenditure burden put on a fresher or any other student by students;<br>
			Any act of physical abuse including all variants of it: sexual abuse, homosexual assaults, stripping, forcing obscene and lewd acts, gestures, causing bodily harm or any other danger to health or person;<br>
			Any act or abuse by spoken words, emails, posts, public insults which would also include deriving perverted pleasure, vicarious or sadistic thrill from actively or passively participating in the discomfiture to fresher or any other student;
			Any act that affects the mental health and self-confidence of a fresher or any other student with or without an intent to derive a sadistic pleasure or showing off power, authority or superiority by a student over any fresher or any other student.<br>
			<strong>Actions to be taken against students for indulging and abetting ragging in technical institutions/Universities including Deemed to be University imparting technical education</strong><br>
			The punishment to be meted out to the persons indulged in ragging has to be exemplary and justifiably harsh to act as a deterrent against recurrence of such incidents.<br>
			Every single incident of ragging a First Information Report (FIR) must be filed without exception by the institutional authorities with the local police authorities.<br>
			The Anti-Ragging Committee of the institution shall take an appropriate decision, with regard to punishment or otherwise, depending on the facts of each incident of ragging and nature and gravity of the incident of ragging.
			Depending upon the nature and gravity of the offence as established the possible punishments for those found guilty of ragging at the institution level shall be any one or any combination of the following,<br><br>
			Cancellation of admission<br>
			Suspension from attending classes<br>
			Withholding / withdrawing scholarship / fellowship and other benefits
			Debarring from appearing in any test / examination or other evaluation process
			Withholding results<br>
			Debarring from representing the institution in any regional, national or international meet, tournament, youth festival, etc.<br>
			Suspension / expulsion from the hostel<br>
			Rustication from the institution for period ranging from 1 to 4 semesters
			Expulsion from the institution and consequent debarring from admission to any other institution.<br>
			Collective punishment: when the persons committing or abetting the crime of ragging are not identified, the institution shall resort to collective punishment as a deterrent to ensure community pressure on the potential raggers.<br><br>
			<strong>ANTI-RAGGING MEASURESSUBMISSION OF
			AFFIDAVITS BY THE STUDENTS / PARENT / GUARDIAN SEEKING ADMISSION TO THE INSTITUTE</strong><br> Dear Parents/Guardian/Student, You are fully aware of the
			orders of the Government and of Hon’ble Supreme Court on the Anti-Ragging
			measures. As per the latest policy all students and parent/guardians are
			required to submit affidavits before a student is allowed registration in the
			institute at the time of admission. The procedure for
			submission of the said affidavit ONLINE is detailed below. In
			case a student does not submit the same he/she shall not be allowed to proceed
			with the registration.<br><br> It is further, requested that this information be passed
			amongst friends. Best wishes, Prof.(Dr.) Asit Kumar Saha, Principal, Haldia Institute of
			Technology, Haldia, WBSteps for filling up the ONLINE anti-ragging affidavit It is a
			simple procedure comprising 5 steps<br>
			Step 1: Log on to 
			http://www.amanmovement.org<br><br>

			Step 2: Click on the button called – Online Undertaking
			(Affidavit)<br><br>

			Step 3: Fill in the information as desired and submit the form.<br><br>

			Step 4: On successful completion you will receive affidavits,
			both for Students and Parents, through E mail. If you do not have an E mail
			address please create one before you log in. If your parents do not have an E
			Mail/Mobile/ Landline Phone number please do not panic. You can give those of
			your friends or relatives. There is absolutely nothing to worry. If you make a
			mistake while submitting your form you can start a fresh and submit the
			information again. There is no problem. It is a very easy process.<br><br>

			Step 5: Take print of both forms, yours own
			and parents’, sign in proper places and submit to the
			institute at the time of admission.<br><br>

			You are required to have the following information before filling
			up the form:<br><br>
			Student details <br> *Student’s last name : *Student’s
			first name : *Student’s mobile number : *Student’s friends mobile no in case of
			emergency : *Landline number : *Student’s email ID : *Permanent Address 1 :
				<br><br>
			Parent/ Guardian details *Parent/Guardian Name :
			*Parent/Guardian Address 1 : *Residence Phone No : *Mobile No of Parent/guardian
			: *Parent/guardian’s Email ID:
<br><br>
			College details
<br>
			*State in which college is : West Bengal<br> *Is it a professional
			or general college: Professional <br>*Name of the college : 
			Haldia Institute of Technology<br> *Name of Affiliating University : 
			Maulana Abul Kalam Azad University of technology (Formerly WBUT) <br>*Is it deemed university : 
			NO <br>*Director/Principal last name : Saha
			*Director/Principal first name : Asit Kumar *Director/Principal
			gender : Male<br><br>

			*College phone no 1 : 03224 252900 *Nearest police station name
			and address : Haldia Police Station, Chiranjibpur, Haldia


		</p>
		
	</div>
	<div id = "arc_table">
		<h1>Anti-Ragging Helpline</h1>
		<table style="width:100%">
			  <tr>
				<th>Contact Person</th>
				<th>Contact Number</th> 
			  </tr>
			  <tr>
				<td>Principal</td>
				<td>03224 252900</td>
			  </tr>
			  <tr>
				<td>Registrar</td>
				<td>9434320005</td>
			  </tr>
			  <tr>
				<td>Convenor, Anti-Ragging Committee</td>
				<td>9432060527</td>
			  </tr>
			  <tr>
				<td>Provost-Hostels</td>
				<td>9434660909</td>
			  </tr>
			
			  <tr>
				<td>Hostel Superintendent</td>
				<td>9434940780</td>
			  </tr>
			  <tr>
				<td>Hostel Caretaker</td>
				<td>9474068554/9933897841 / 9547129810</td>
			  </tr>
			  <tr>
				<td>D. O. Haldia</td>
				<td>03224 252478</td>
			  </tr>
			  <tr>
				<td>D. P. O Haldia</td>
				<td>03224 2278109</td>
			  </tr>
			  <tr>
				<td>Haldia Police Station</td>
				<td>03224 2521112</td>
			  </tr>
		</table>
		<h1>IT Dept - Anti Ragging Cell</h1>
		<table>
			  <tr>
				<th>Contact Person</th>
				<th>Contact Number</th> 
			  </tr>
			  <tr>
				<td><strong>Fourth Year</strong></td>
			  </tr>
			  <tr>
				<td>Rahul Kumar</td>
				<td>9563398523</td>
			  </tr>
			  <tr>
				<td>Richa Jaiswal</td>
				<td>9932072486</td>
			  </tr>
			  <tr>
				<td><strong>Third Year</strong></td>
			  </tr>
			  <tr>
				<td>Ankit Anand</td>
				<td>8084944111</td>
			  </tr>
			  <tr>
				<td>Shilpi Suman</td>
				<td>9432060527</td>
			  </tr>
				<td><strong>Second Year</strong></td>
			  </tr>
			  <tr>
				<td>*******</td>
				<td>**********</td>
			  </tr>
			  <tr>
				<td>************</td>
				<td>***********</td>
			  </tr>
			  <tr>
		</table>	
	</div>

</section>
<?php
include("footer.php");
?>