<style>
.footer {
   left: 0;
   bottom: 0;
   width: 100%;
   background-color: #619af4;
   color: red;
}
.footer p{
  text-align: left;
  padding-left:15px;
  padding-bottom: 2px;
}
.footer pre{
  background-color: #335b9b;
  text-align: left;
  padding:5px 0 5px 0; 
  bottom: 0;
  width:100%;
  text-align: center;
}

input[type=email], textarea {
    width: 300px;
    padding: 5px;
    border: 1px solid #ccc;
    border-radius: 3px;
    box-sizing: border-box;
    margin-top: 2px;
    margin-bottom: 5px;
    resize: vertical;
}

input[type=submit] {
    background-color: #4CAF50;
    padding: 12px 20px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

input[type=submit]:hover {
    background-color: #45a049;
}

.footer-right{
    border-radius: 5px;
    padding: 15px;
    float: right;
    right: 0;
    margin-top:0; 
}
.footer-left{
  float: left;
}
</style>
	  <footer>
<div class="footer">
  <div class="footer-left">
	  <p>Information Technology Department</br> Haldia Institute of Technology</br> Haldia, 721657</p>
  <br>
  <p>Tel: +91 3224 252800</br>
  Email: contactus@ithit.com</p><br><br>
  </div>
  <div class="footer-right">
    <h3>Contact Us</h3>
    <form action="#">
      <textarea id="subject" name="subject" placeholder="Your Message...." style="height:100px"></textarea><br>
      <input type="email" id="email" name="emailid" placeholder="Email Address"><br>
      <input type="submit" value="Submit">
    </form>
	 
  </div>
  <pre>Copyright&copy;2018-All Rights reserved-ithit.com</pre>
</div>
 </footer>
</body>
</html> 